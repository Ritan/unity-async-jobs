﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UnityAsyncJobs;

namespace Example
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Out.WriteLine("Main thread id: {0}", Thread.CurrentThread.ManagedThreadId);

            AsyncExecutor ex = new AsyncExecutor();
            while (true)
            {
                Job j = new Job((object stub1) =>
                {
                    Thread.Sleep(500);
                    Console.Out.WriteLine("Job execution finished on thread {0}", Thread.CurrentThread.ManagedThreadId);
                },
                (object stub2) =>
                {
                    Console.Out.WriteLine("Callback called on thread {0}", Thread.CurrentThread.ManagedThreadId);
                });

                ex.Submit(j);
                Console.Out.WriteLine("Job submitted");
                Thread.Sleep(100);
                ex.HandleCallbacks();
            }
        }
    }
}
