﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityAsyncJobs
{
    public class Job
    {
        private Action<Object> _action;
        private Action<Object> _handleCompleted;


        public Job(Action<Object> toRun, Action<Object> handleCompleted)
        {
            _action = toRun;
            _handleCompleted = handleCompleted;
        }

        public Action<Object> Execute()
        {
            _action(null);
            return _handleCompleted;
        }
    }
}
