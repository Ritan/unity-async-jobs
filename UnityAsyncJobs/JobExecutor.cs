﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace UnityAsyncJobs
{
    public abstract class JobExecutor
    {
        public abstract void Submit(Job j);
        public abstract void HandleCallbacks();
    }
}
