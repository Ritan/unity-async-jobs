﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace UnityAsyncJobs
{
    public class AsyncExecutor : JobExecutor
    {
        private Queue<Action<Object>> _pendingCompleted;
        private Queue<Job> _queue;

        private Thread _thread;

        private bool _done = false;

        public AsyncExecutor()
        {
            _queue = new Queue<Job>();
            _pendingCompleted = new Queue<Action<object>>();

            _thread = new Thread(() =>
            {
                while (!_done)
                {
                    Action<Object> callback;
                    Job j;
                    lock (_queue)
                    {
                        j = _queue.Dequeue();
                    };

                    callback = j.Execute();

                    lock (_pendingCompleted)
                    {
                        _pendingCompleted.Enqueue(callback);
                    }
                }
            });

            _thread.Start();
        }

        ~AsyncExecutor()
        {
            _thread.Abort();
        }

        public override void Submit(Job j)
        {
            _queue.Enqueue(j);
        }

        public override void HandleCallbacks()
        {
            lock (_pendingCompleted)
            {
                //TODO более вменяемый способ пройти все колбеки
                while (_pendingCompleted.Count != 0)
                {
                    Action<Object> callback = _pendingCompleted.Dequeue();
                    callback(null);
                }
            }
        }

    }
}
