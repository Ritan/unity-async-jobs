﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace UnityAsyncJobs
{
    public class ThreadPoolExecutor : JobExecutor
    {
        private Queue<Action<Object>> _pendingCompleted;

        public ThreadPoolExecutor()
        {
            _pendingCompleted = new Queue<Action<object>>();
        }

        ~ThreadPoolExecutor()
        {
        }

        public override void HandleCallbacks()
        {
            while (_pendingCompleted.Count != 0)
            {
                Action<Object> callback = _pendingCompleted.Dequeue();
                callback(null);
            }
        }

        public override void Submit(Job j)
        {
            ThreadPool.QueueUserWorkItem((object stub) =>
            {
                Action<object> callback = j.Execute();
                lock (_pendingCompleted)
                {
                    _pendingCompleted.Enqueue(callback);
                }
            });
        }
    }
}
